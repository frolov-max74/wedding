<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `guest`.
 */
class m170419_180456_add_created_at_column_to_guest_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%guest}}', 'created_at', $this->integer()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%guest}}', 'created_at');
    }
}
