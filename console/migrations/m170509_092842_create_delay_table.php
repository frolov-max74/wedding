<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delay`.
 */
class m170509_092842_create_delay_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%delay}}', [
            'id' => $this->primaryKey(),
            'value' => $this->char(4)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%delay}}');
    }
}
