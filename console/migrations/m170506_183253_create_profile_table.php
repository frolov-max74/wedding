<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m170506_183253_create_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(32)->notNull(),
            'last_name' => $this->string(32)->notNull(),
            'avatar' => $this->string(50),
            'website' => $this->string(32),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-profile-user_id', '{{%profile}}', 'user_id');
        $this->addForeignKey('fk-profile-user', '{{%profile}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%profile}}');
    }
}
