<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gallery`.
 */
class m170509_093621_create_gallery_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%gallery}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(50)->notNull(),
            'wedding_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-gallery-wedding_id', '{{%gallery}}', 'wedding_id');
        $this->addForeignKey('fk-gallery-wedding', '{{%gallery}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%gallery}}');
    }
}
