<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stories`.
 */
class m170509_104923_create_stories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%stories}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->integer()->notNull(),
            'image' => $this->string(50)->notNull(),
            'img_animate_id' => $this->integer()->notNull(),
            'txt_animate_id' => $this->integer()->notNull(),
            'img_delay_id' => $this->integer()->notNull(),
            'txt_delay_id' => $this->integer()->notNull(),
            'wedding_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-stories-img_animate_id', '{{%stories}}', 'img_animate_id');
        $this->createIndex('idx-stories-txt_animate_id', '{{%stories}}', 'txt_animate_id');
        $this->createIndex('idx-stories-img_delay_id', '{{%stories}}', 'img_delay_id');
        $this->createIndex('idx-stories-txt_delay_id', '{{%stories}}', 'txt_delay_id');
        $this->createIndex('idx-stories-wedding_id', '{{%stories}}', 'wedding_id');

        $this->addForeignKey('fk-stories-img_animate', '{{%stories}}', 'img_animate_id', '{{%animate}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-stories-txt_animate', '{{%stories}}', 'txt_animate_id', '{{%animate}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-stories-img_delay', '{{%stories}}', 'img_delay_id', '{{%delay}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-stories-txt_delay', '{{%stories}}', 'txt_delay_id', '{{%delay}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-stories-wedding', '{{%stories}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%stories}}');
    }
}
