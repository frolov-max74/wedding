<?php

use yii\db\Migration;

/**
 * Handles the creation of table `animate`.
 */
class m170509_092803_create_animate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%animate}}', [
            'id' => $this->primaryKey(),
            'name' => $this->char(12)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%animate}}');
    }
}
