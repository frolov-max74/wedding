<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place`.
 */
class m170509_111133_create_place_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%place}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'lead' => $this->string(100)->notNull(),
            'image' => $this->string(50)->notNull(),
            'street_house' => $this->string(100)->notNull(),
            'city' => $this->string(32)->notNull(),
            'animate_id' => $this->integer()->notNull(),
            'delay_id' => $this->integer()->notNull(),
            'wedding_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-place-animate_id', '{{%place}}', 'animate_id');
        $this->createIndex('idx-place-delay_id', '{{%place}}', 'delay_id');
        $this->createIndex('idx-place-wedding_id', '{{%place}}', 'wedding_id');

        $this->addForeignKey('fk-place-animate', '{{%place}}', 'animate_id', '{{%animate}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-place-delay', '{{%place}}', 'delay_id', '{{%delay}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-place-wedding', '{{%place}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%place}}');
    }
}
