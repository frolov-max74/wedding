<?php

use yii\db\Migration;

/**
 * Handles the creation of table `empty_bgs`.
 */
class m170522_115120_create_empty_bgs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%empty_bgs}}', [
            'id' => $this->primaryKey(),
            'first_bg' =>$this->string(50)->notNull(),
            'second_bg' =>$this->string(50)->notNull(),
            'third_bg' =>$this->string(50)->notNull(),
            'fourth_bg' =>$this->string(50)->notNull(),
            'wedding_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-empty_bgs-wedding_id', '{{%empty_bgs}}', 'wedding_id');

        $this->addForeignKey('fk-empty_bgs-wedding', '{{%empty_bgs}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%empty_bgs}}');
    }
}
