<?php

use yii\db\Migration;

/**
 * Handles the creation of table `guest`.
 */
class m170407_101444_create_guest_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%guest}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'surname' => $this->string(32)->notNull(),
            'email' => $this->string(128)->notNull()->unique(),
            'gender' => $this->char(1)->notNull(),
            'presence' => $this->char(1)->notNull(),
            'message' => $this->text()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%guest}}');
    }
}
