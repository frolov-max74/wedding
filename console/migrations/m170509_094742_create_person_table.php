<?php

use yii\db\Migration;

/**
 * Handles the creation of table `person`.
 */
class m170509_094742_create_person_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%person}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'surname' => $this->string(32),
            'text' => $this->text(),
            'image' => $this->string(50)->notNull(),
            'status_id' => $this->integer()->notNull(),
            'wedding_id' => $this->integer()->notNull(),
            'animate_id' => $this->integer()->notNull(),
            'delay_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-person-status_id', '{{%person}}', 'status_id');
        $this->createIndex('idx-person-wedding_id', '{{%person}}', 'wedding_id');
        $this->createIndex('idx-person-animate_id', '{{%person}}', 'animate_id');
        $this->createIndex('idx-person-delay_id', '{{%person}}', 'delay_id');

        $this->addForeignKey('fk-person-status', '{{%person}}', 'status_id', '{{%status}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-person-wedding', '{{%person}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-person-animate', '{{%person}}', 'animate_id', '{{%animate}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-person-delay', '{{%person}}', 'delay_id', '{{%delay}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%person}}');
    }
}
