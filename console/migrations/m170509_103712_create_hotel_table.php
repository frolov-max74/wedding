<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hotel`.
 */
class m170509_103712_create_hotel_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%hotel}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'street_house' => $this->string(100)->notNull(),
            'city' => $this->string(32)->notNull(),
            'image' => $this->string(50)->notNull(),
            'animate_id' => $this->integer()->notNull(),
            'delay_id' => $this->integer()->notNull(),
            'wedding_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-hotel-animate_id', '{{%hotel}}', 'animate_id');
        $this->createIndex('idx-hotel-delay_id', '{{%hotel}}', 'delay_id');
        $this->createIndex('idx-hotel-wedding_id', '{{%hotel}}', 'wedding_id');

        $this->addForeignKey('fk-hotel-animate', '{{%hotel}}', 'animate_id', '{{%animate}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-hotel-delay', '{{%hotel}}', 'delay_id', '{{%delay}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-hotel-wedding', '{{%hotel}}', 'wedding_id', '{{%wedding}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%hotel}}');
    }
}
