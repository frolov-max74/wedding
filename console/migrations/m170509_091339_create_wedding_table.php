<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wedding`.
 */
class m170509_091339_create_wedding_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%wedding}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'groom_bride' => $this->text()->notNull(),
            'story' => $this->text()->notNull(),
            'details' => $this->text()->notNull(),
            'attending' => $this->text()->notNull(),
            'accommodation_top' =>$this->text()->notNull(),
            'accommodation_down' =>$this->text()->notNull(),
            'gallery' => $this->text()->notNull(),
            'rsvp' => $this->text()->notNull(),
            'status' => $this->char(1)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-wedding-status', '{{%wedding}}', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%wedding}}');
    }
}
