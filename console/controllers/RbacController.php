<?php

namespace console\controllers;

use common\rbac\rules\DeleteOwnProfile;
use Yii;
use yii\console\Controller;
use common\rbac\rules\DeleteUserRule;
use common\rbac\rules\UserRoleRule;
use common\rbac\rules\UpdateOwnProfile;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // removed old data
        $auth->removeAll();

        // added permission "dashboard"
        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'Dashboard';
        $auth->add($dashboard);

        // added rule "DeleteUserRule"
        $rule = new DeleteUserRule();
        $auth->add($rule);

        // added permission "deleteUser"
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $deleteUser->ruleName = $rule->name;
        $auth->add($deleteUser);

        // added rule "UpdateOwnProfile"
        $rule = new UpdateOwnProfile();
        $auth->add($rule);

        // added permission "updateOwnProfile"
        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->description = 'Update own profile';
        $updateOwnProfile->ruleName = $rule->name;
        $auth->add($updateOwnProfile);

        // added rule "DeleteOwnProfile"
        $rule = new DeleteOwnProfile();
        $auth->add($rule);

        // added permission "deleteOwnProfile"
        $deleteOwnProfile = $auth->createPermission('deleteOwnProfile');
        $deleteOwnProfile->description = 'Delete own profile';
        $deleteOwnProfile->ruleName = $rule->name;
        $auth->add($deleteOwnProfile);

        // added rule "UserRoleRule"
        $rule = new UserRoleRule();
        $auth->add($rule);

        // added the role of "user" and is bound to her rule "UserRoleRule"
        $user = $auth->createRole('user');
        $user->description = 'User';
        $user->ruleName = $rule->name;
        $auth->add($user);

        // added the role of "owner" and is bound to her rule "UserRoleRule"
        $owner = $auth->createRole('owner');
        $owner->description = 'Owner';
        $owner->ruleName = $rule->name;
        $auth->add($owner);

        // added the role of "admin" and is bound to her rule "UserRoleRule"
        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        // added permissions to roles
        $auth->addChild($owner, $dashboard);
        $auth->addChild($owner, $updateOwnProfile);
        $auth->addChild($owner, $deleteOwnProfile);
        $auth->addChild($admin, $deleteUser);

        // also all the permissions of "user" to the "owner"
        $auth->addChild($owner, $user);

        // also all the permissions of "owner" to the "admin"
        $auth->addChild($admin, $owner);

        // assigned roles to users
        $auth->assign($admin, 1);
        $auth->assign($owner, 3);
        // ... here assign roles to other users

        $this->stdout('Done!' . PHP_EOL, Console::FG_CYAN, Console::UNDERLINE);
    }
}
