<?php

use yii\helpers\Html;

/* @var $guest string */
/* @var $email string */
/* @var $message string */
?>
<div>
    <p><strong><?= Html::encode($guest) ?></strong> успешно зарегистрирован(а) на Вашем мероприятии.</p>

    <p><strong>Email:</strong> <?= Html::encode($email) ?></p>

    <?php if ('' !== $message): ?>
        <p><strong>Сообщение:</strong> «<?= html::encode($message) ?>»</p>
    <?php endif; ?>
</div>
