<?php

namespace common\components;

use yii\base\Component;
use yii\imagine\Image as Img;

class Image extends Component
{
    /**
     * @param string $srcImage
     * @param string $destImage
     * @param int $width
     * @param int $height
     * @param int $quality
     * @return bool
     */
    public function resizeImage(string $srcImage, string $destImage, int $width, int $height, int $quality = 100): bool
    {
        if (!file_exists($srcImage)) {
            return false;
        } else {
            Img::thumbnail($srcImage, $width, $height)
                ->save($destImage, ['quality' => $quality]);
            return true;
        }
    }
}
