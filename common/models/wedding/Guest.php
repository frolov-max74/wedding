<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%guest}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $gender
 * @property string $presence
 * @property string $message
 * @property integer $created_at
 */
class Guest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guest}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'gender', 'presence', 'created_at'], 'required'],
            [['created_at'], 'integer'],
            [['message'], 'string'],
            [['message'], 'default', 'value' => ''],
            [['name', 'surname'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 128],
            [['gender', 'presence'], 'string', 'max' => 1],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'gender' => 'Gender',
            'presence' => 'Presence',
            'message' => 'Message',
            'created_at' => 'Created At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        // Attaching the event handler
        $this->on(
            self::EVENT_AFTER_INSERT,
            [$this, 'sendEmail'],
            [
                'emailTo' => Yii::$app->params['adminEmail'],
                'emailFrom' => Yii::$app->params['supportEmail'],
            ]
        );

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Sends email to the administrator, receives the data from the object that initiated this event.
     *
     * @param $event
     * @return bool
     */
    protected function sendEmail($event)
    {
        $emailTo = $event->data['emailTo'];
        $emailFrom = $event->data['emailFrom'];
        $guest = $event->sender;

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'rsvp-html', 'text' => 'rsvp-text'],
                [
                    'guest' => $this->getFullName($guest),
                    'email' => $guest->email,
                    'message' => $guest->message,
                ]
            )
            ->setTo($emailTo)
            ->setFrom([$emailFrom => $this->getFullName($guest)])
            ->setReplyTo([$guest->email => $this->getFullName($guest)])
            ->setSubject('You have a new guest..!')
            ->send();
    }

    /**
     * @param Guest $guest
     * @return string full name
     */
    protected function getFullName(self $guest)
    {
        return $guest->name . ' ' . $guest->surname;
    }

}
