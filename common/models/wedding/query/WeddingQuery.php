<?php

namespace common\models\wedding\query;

use common\models\wedding\Wedding;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\wedding\Wedding]].
 *
 * @see \common\models\wedding\Wedding
 */
class WeddingQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['status' => Wedding::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     * @return \common\models\wedding\Wedding[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\wedding\Wedding|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
