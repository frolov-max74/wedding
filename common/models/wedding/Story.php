<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%stories}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $date
 * @property string $image
 * @property integer $img_animate_id
 * @property integer $txt_animate_id
 * @property integer $img_delay_id
 * @property integer $txt_delay_id
 * @property integer $wedding_id
 *
 * @property Animate $imgAnimate
 * @property Delay $imgDelay
 * @property Animate $txtAnimate
 * @property Delay $txtDelay
 * @property Wedding $wedding
 */
class Story extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'date', 'image', 'img_animate_id', 'txt_animate_id', 'img_delay_id', 'txt_delay_id', 'wedding_id'], 'required'],
            [['text'], 'string'],
            [['img_animate_id', 'txt_animate_id', 'img_delay_id', 'txt_delay_id', 'wedding_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['date'], 'date', 'format' => 'dd-MM-yyyy', 'timestampAttribute' => 'date'],
            [['image'], 'string', 'max' => 50],
            [['title', 'text'], 'trim'],
            [['imageFile'], 'file', 'maxSize' => 2048 * 2048, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['img_animate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Animate::className(), 'targetAttribute' => ['img_animate_id' => 'id']],
            [['img_delay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delay::className(), 'targetAttribute' => ['img_delay_id' => 'id']],
            [['txt_animate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Animate::className(), 'targetAttribute' => ['txt_animate_id' => 'id']],
            [['txt_delay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delay::className(), 'targetAttribute' => ['txt_delay_id' => 'id']],
            [['wedding_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wedding::className(), 'targetAttribute' => ['wedding_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'date' => 'Date',
            'imageFile' => 'Image',
            'img_animate_id' => 'Img Animate ID',
            'txt_animate_id' => 'Txt Animate ID',
            'img_delay_id' => 'Img Delay ID',
            'txt_delay_id' => 'Txt Delay ID',
            'wedding_id' => 'Wedding ID',
        ];
    }

    /**
     * Upload and resizing image
     */
    public function uploadImage()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'uploads/wedding/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteImageFile();
                $image = new Image();
                $thumbPath = 'uploads/wedding/images/story-' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 681, 685, 80)) {
                    $this->image = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     */
    protected function deleteImageFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->image))) {
            unlink(Yii::getAlias('@frontend/web' . $this->image));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgAnimate()
    {
        return $this->hasOne(Animate::className(), ['id' => 'img_animate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgDelay()
    {
        return $this->hasOne(Delay::className(), ['id' => 'img_delay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTxtAnimate()
    {
        return $this->hasOne(Animate::className(), ['id' => 'txt_animate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTxtDelay()
    {
        return $this->hasOne(Delay::className(), ['id' => 'txt_delay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWedding()
    {
        return $this->hasOne(Wedding::className(), ['id' => 'wedding_id']);
    }
}
