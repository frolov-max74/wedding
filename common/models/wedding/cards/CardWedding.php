<?php

namespace common\models\wedding\cards;

use common\models\wedding\db\Gallery;
use common\models\wedding\db\Hotel;
use common\models\wedding\db\Person;
use common\models\wedding\db\Place;
use common\models\wedding\db\Story;

/**
 * Class CardWedding
 * @package common\models\wedding\cards
 */
class CardWedding
{
    /**
     * @param $data array
     * @return array
     */
    public static function findFull($data)
    {
        $result = [
            'title'        => $data['title'],
            'groom_bride'  => $data['groom_bride'],
            'story_text'   => $data['story'],
            'details'      => $data['details'],
            'attending'    => $data['attending'],
            'hotel_top'    => $data['accommodation_top'],
            'hotel_down'   => $data['accommodation_down'],
            'gallery_text' => $data['gallery'],
            'rsvp'         => $data['rsvp'],
            'first_bg'     => $data['first_bg'],
            'second_bg'    => $data['second_bg'],
            'third_bg'     => $data['third_bg'],
            'fourth_bg'    => $data['fourth_bg'],
            'gallery'      => Gallery::findAll(),
            'hotels'       => Hotel::findAll(),
            'places'       => Place::findAll(),
            'story'        => Story::findAll(),
            'groom'        => Person::findGroom(),
            'bride'        => Person::findBride(),
            'buddies'      => Person::findBuddies(),
            'bridesmaids'  => Person::findBridesmaids(),
        ];

        $result['groom']['full_name']  = $result['groom']['name'] . ' ' . $result['groom']['surname'];
        $result['bride']['full_name']  = $result['bride']['name'] . ' ' . $result['bride']['surname'];

        foreach ($result['story'] as &$item) {
            $item['day'] = date('d', $item['date']);
            $item['month_year'] = date('m-y', $item['date']);
        }

        return $result;
    }
}
