<?php

namespace common\models\wedding;

use yii\base\Model;

class GendersList extends Model
{
    public static $names = [
        'Женский',
        'Мужской',
        'Я сомневаюсь в выборе',
    ];
}
