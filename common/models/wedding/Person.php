<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%person}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $text
 * @property string $image
 * @property integer $status_id
 * @property integer $wedding_id
 * @property integer $animate_id
 * @property integer $delay_id
 *
 * @property Animate $animate
 * @property Delay $delay
 * @property Status $status
 * @property Wedding $wedding
 */
class Person extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image', 'status_id', 'wedding_id', 'animate_id', 'delay_id'], 'required'],
            [['text'], 'string'],
            [['status_id', 'wedding_id', 'animate_id', 'delay_id'], 'integer'],
            [['name', 'surname'], 'string', 'max' => 32],
            [['image'], 'string', 'max' => 50],
            [['name', 'surname', 'text'], 'trim'],
            [['imageFile'], 'file', 'maxSize' => 2048 * 2048, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['animate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Animate::className(), 'targetAttribute' => ['animate_id' => 'id']],
            [['delay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delay::className(), 'targetAttribute' => ['delay_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['wedding_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wedding::className(), 'targetAttribute' => ['wedding_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'text' => 'Text',
            'imageFile' => 'Image',
            'status_id' => 'Status ID',
            'wedding_id' => 'Wedding ID',
            'animate_id' => 'Animate ID',
            'delay_id' => 'Delay ID',
        ];
    }

    /**
     * Upload and resizing image
     */
    public function uploadImage()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'uploads/wedding/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteImageFile();
                $image = new Image();
                $thumbPath = 'uploads/wedding/images/person-' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 500, 500, 80)) {
                    $this->image = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     */
    protected function deleteImageFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->image))) {
            unlink(Yii::getAlias('@frontend/web' . $this->image));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnimate()
    {
        return $this->hasOne(Animate::className(), ['id' => 'animate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelay()
    {
        return $this->hasOne(Delay::className(), ['id' => 'delay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWedding()
    {
        return $this->hasOne(Wedding::className(), ['id' => 'wedding_id']);
    }

    /**
     * @return string full name
     */
    public function getFullName()
    {
        return !$this->surname ? $this->name : $this->name . ' ' . $this->surname;
    }
}
