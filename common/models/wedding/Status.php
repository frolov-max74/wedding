<?php

namespace common\models\wedding;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%status}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Person[] $people
 */
class Status extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 12],
            [['name'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['status_id' => 'id']);
    }

    /**
     * @return array array of the names of the statuses
     */
    public static function getAll()
    {
        return self::find()->select(['name', 'id'])->indexBy('id')->column();
    }
}
