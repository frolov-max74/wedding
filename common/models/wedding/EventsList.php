<?php

namespace common\models\wedding;

use yii\base\Model;

/**
 * Class EventsList
 * @package common\models\wedding
 */
class EventsList extends Model
{
    static $names = [
        'Все события',
        'Главная церемония',
        'Праздничный вечер',
    ];
}
