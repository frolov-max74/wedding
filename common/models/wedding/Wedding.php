<?php

namespace common\models\wedding;

use yii\db\ActiveRecord;
use common\models\wedding\query\WeddingQuery;

/**
 * This is the model class for table "{{%wedding}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $groom_bride
 * @property string $story
 * @property string $details
 * @property string $attending
 * @property string $accommodation_top
 * @property string $accommodation_down
 * @property string $gallery
 * @property string $rsvp
 * @property string $status
 *
 * @property Gallery[] $galleries
 * @property Hotel[] $hotels
 * @property Person[] $people
 * @property Place[] $places
 * @property Story[] $stories
 */
class Wedding extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wedding}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'groom_bride', 'story', 'details', 'attending', 'accommodation_top', 'accommodation_down', 'gallery', 'rsvp', 'status'], 'required'],
            [['groom_bride', 'story', 'details', 'attending', 'accommodation_top', 'accommodation_down', 'gallery', 'rsvp'], 'string'],
            [['title', 'groom_bride', 'story', 'details', 'attending', 'accommodation_top', 'accommodation_down', 'gallery', 'rsvp'], 'trim'],
            [['title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 1],
            ['status', 'in', 'range' => [self::STATUS_INACTIVE, self::STATUS_ACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'groom_bride' => 'Groom Bride',
            'story' => 'Story',
            'details' => 'Details',
            'attending' => 'Attending',
            'accommodation_top' => 'Accommodation Top',
            'accommodation_down' => 'Accommodation Down',
            'gallery' => 'Gallery',
            'rsvp' => 'Rsvp',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries()
    {
        return $this->hasMany(Gallery::className(), ['wedding_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['wedding_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['wedding_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces()
    {
        return $this->hasMany(Place::className(), ['wedding_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories()
    {
        return $this->hasMany(Story::className(), ['wedding_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\wedding\query\WeddingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WeddingQuery(get_called_class());
    }

    /**
     * @return array|Wedding|null
     */
    public static function findActive()
    {
        return self::find()->active()->one();
    }

    /**
     * @return bool
     */
    public function checkCountActive()
    {
        return (
            count(self::find()->active()->one()) > 0 &&
            $this->status == self::STATUS_ACTIVE
        ) ? false : true;
    }

    /**
     * @return array all ids
     */
    public static function findAllIds()
    {
        return self::find()
            ->select('id')
            ->orderBy('id')
            ->indexBy('id')
            ->column();
    }
}
