<?php

namespace common\models\wedding;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\components\Image;

class NonEmptyBgs extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    const HOME_PATH = '/uploads/wedding/images/bg-home.jpg';
    const MARRIED_PATH = '/uploads/wedding/images/bg-get-married.jpg';
    const THANKS_PATH = '/uploads/wedding/images/bg-thanks.jpg';

    /**
     * @return array validation rules
     */
    public function rules()
    {
        return [
            [
                ['imageFiles'],
                'file',
                'skipOnEmpty' => false,
                'maxSize' => 2048 * 2048,
                'extensions' => 'jpg',
                'checkExtensionByMimeType' => false,
                'maxFiles' => 3
            ],
            [
                ['imageFiles'],
                function () {
                    if (count($this->imageFiles) != 3) {
                        $this->addError(
                            'imagesFiles',
                            'Необходимо загрузить 3 фоновых изображения.'
                        );
                    }
                },
            ],
        ];
    }

    /**
     * @return array attributes labels
     */
    public function attributeLabels()
    {
        return [
            'imageFiles' => 'Image Files',
        ];
    }

    /**
     * Upload and resizing background images
     */
    public function uploadImages()
    {
        $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');
        $basePath = Yii::getAlias('@frontend/web/');
        if ($this->validate()) {
            foreach ($this->imageFiles as $item => $file) {
                $srcPath = $basePath . 'uploads/wedding/images/' . $file->baseName . '.' . $file->extension;
                if ($file->saveAs($srcPath)) {
                    $image = new Image();
                    switch ($item) {
                        case 0:
                            $thumbPath = 'uploads/wedding/images/bg-home.jpg';
                            $destPath = $basePath . $thumbPath;
                            $image->resizeImage($srcPath, $destPath, 1920, 1000);
                            unlink($srcPath);
                            break;
                        case 1:
                            $thumbPath = 'uploads/wedding/images/bg-get-married.jpg';
                            $destPath = $basePath . $thumbPath;
                            $image->resizeImage($srcPath, $destPath, 1920, 1000);
                            unlink($srcPath);
                            break;
                        case 2:
                            $thumbPath = 'uploads/wedding/images/bg-thanks.jpg';
                            $destPath = $basePath . $thumbPath;
                            $image->resizeImage($srcPath, $destPath, 1920, 1000);
                            unlink($srcPath);
                            break;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getHomePath()
    {
        return file_exists(Yii::getAlias('@frontend/web') . self::HOME_PATH) ?
            Yii::getAlias('@frontend/web') . self::HOME_PATH :
            false;
    }

    /**
     * @return bool|string
     */
    public function getMarriedPath()
    {
        return file_exists(Yii::getAlias('@frontend/web') . self::MARRIED_PATH) ?
            Yii::getAlias('@frontend/web') . self::MARRIED_PATH :
            false;
    }

    /**
     * @return bool|string
     */
    public function getThanksPath()
    {
        return file_exists(Yii::getAlias('@frontend/web') . self::THANKS_PATH) ?
            Yii::getAlias('@frontend/web') . self::THANKS_PATH :
            false;
    }

    /**
     * @return string
     */
    public function getHomeUrl()
    {
        return Yii::$app->params['baseUrl'] . self::HOME_PATH;
    }

    /**
     * @return string
     */
    public function getMarriedUrl()
    {
        return Yii::$app->params['baseUrl'] . self::MARRIED_PATH;
    }

    /**
     * @return string
     */
    public function getThanksUrl()
    {
        return Yii::$app->params['baseUrl'] . self::THANKS_PATH;
    }

}
