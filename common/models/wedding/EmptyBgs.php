<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%empty_bgs}}".
 *
 * @property integer $id
 * @property string $first_bg
 * @property string $second_bg
 * @property string $third_bg
 * @property string $fourth_bg
 * @property integer $wedding_id
 *
 * @property Wedding $wedding
 */
class EmptyBgs extends ActiveRecord
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%empty_bgs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_bg', 'second_bg', 'third_bg', 'fourth_bg', 'wedding_id'], 'required'],
            [['wedding_id'], 'integer'],
            [
                ['imageFiles'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => 'png, jpg',
                'checkExtensionByMimeType' => false,
                'maxFiles' => 4
            ],
            [
                ['imageFiles'],
                function () {
                    if (count($this->imageFiles) != 4) {
                        $this->addError(
                            'imagesFiles',
                            'Необходимо загрузить 4 фоновых изображения.'
                        );
                    }
                },
            ],
            [['first_bg', 'second_bg', 'third_bg', 'fourth_bg'], 'string', 'max' => 50],
            [
                ['wedding_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Wedding::className(),
                'targetAttribute' => ['wedding_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_bg' => 'First Background',
            'second_bg' => 'Second Background',
            'third_bg' => 'Third Background',
            'fourth_bg' => 'Fourth Background',
            'wedding_id' => 'Wedding ID',
        ];
    }

    /**
     * Upload and resizing background images
     */
    public function uploadImages()
    {
        $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');
        $basePath = Yii::getAlias('@frontend/web/');
        if ($this->validate(['imageFiles'])) {
            foreach ($this->imageFiles as $item => $file) {
                $srcPath = $basePath . 'uploads/wedding/images/' . $file->baseName . '.' . $file->extension;
                if ($file->saveAs($srcPath)) {
                    $image = new Image();
                    $thumbPath = 'uploads/wedding/images/bg-' . $item . time() . '.' . $file->extension;
                    $destPath = $basePath . $thumbPath;
                    if ($image->resizeImage($srcPath, $destPath, 1920, 1000)) {
                        switch ($item) {
                            case 0:
                                $this->deleteFirstBg();
                                $this->first_bg = '/' . $thumbPath;
                                unlink($srcPath);
                                break;
                            case 1:
                                $this->deleteSecondBg();
                                $this->second_bg = '/' . $thumbPath;
                                unlink($srcPath);
                                break;
                            case 2:
                                $this->deleteThirdBg();
                                $this->third_bg = '/' . $thumbPath;
                                unlink($srcPath);
                                break;
                            case 3:
                                $this->deleteFourthBg();
                                $this->fourth_bg = '/' . $thumbPath;
                                unlink($srcPath);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image files
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFiles();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes all backgrounds
     */
    protected function deleteImageFiles()
    {
        $this->deleteFirstBg();
        $this->deleteSecondBg();
        $this->deleteThirdBg();
        $this->deleteFourthBg();
    }

    /**
     * Deletes the first background
     */
    protected function deleteFirstBg()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->first_bg))) {
            unlink(Yii::getAlias('@frontend/web' . $this->first_bg));
        }
    }

    /**
     * Deletes the second background
     */
    protected function deleteSecondBg()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->second_bg))) {
            unlink(Yii::getAlias('@frontend/web' . $this->second_bg));
        }
    }

    /**
     * Deletes the third background
     */
    protected function deleteThirdBg()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->third_bg))) {
            unlink(Yii::getAlias('@frontend/web' . $this->third_bg));
        }
    }

    /**
     * Deletes the fourth background
     */
    protected function deleteFourthBg()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->fourth_bg))) {
            unlink(Yii::getAlias('@frontend/web' . $this->fourth_bg));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWedding()
    {
        return $this->hasOne(Wedding::className(), ['id' => 'wedding_id']);
    }
}
