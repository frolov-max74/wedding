<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%gallery}}".
 *
 * @property integer $id
 * @property string $image
 * @property integer $wedding_id
 *
 * @property Wedding $wedding
 */
class Gallery extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'wedding_id'], 'required'],
            [['wedding_id'], 'integer'],
            [['imageFile'], 'file', 'maxSize' => 2048 * 2048, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['image'], 'string', 'max' => 50],
            [['wedding_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wedding::className(), 'targetAttribute' => ['wedding_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imageFile' => 'Image',
            'wedding_id' => 'Wedding ID',
        ];
    }

    /**
     * Upload and resizing image
     */
    public function uploadImage()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'uploads/wedding/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteImageFile();
                $image = new Image();
                $thumbPath = 'uploads/wedding/images/g-' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 681, 685, 80)) {
                    $this->image = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     */
    protected function deleteImageFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->image))) {
            unlink(Yii::getAlias('@frontend/web' . $this->image));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWedding()
    {
        return $this->hasOne(Wedding::className(), ['id' => 'wedding_id']);
    }
}
