<?php

namespace common\models\wedding;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%place}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $lead
 * @property string $image
 * @property string $street_house
 * @property string $city
 * @property integer $animate_id
 * @property integer $delay_id
 * @property integer $wedding_id
 *
 * @property Animate $animate
 * @property Delay $delay
 * @property Wedding $wedding
 */
class Place extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%place}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'lead', 'image', 'street_house', 'city', 'animate_id', 'delay_id', 'wedding_id'], 'required'],
            [['animate_id', 'delay_id', 'wedding_id'], 'integer'],
            [['title', 'image'], 'string', 'max' => 50],
            [['lead', 'street_house'], 'string', 'max' => 100],
            [['city'], 'string', 'max' => 32],
            [['title', 'lead', 'street_house', 'city'], 'trim'],
            [['imageFile'], 'file', 'maxSize' => 2048 * 2048, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['animate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Animate::className(), 'targetAttribute' => ['animate_id' => 'id']],
            [['delay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delay::className(), 'targetAttribute' => ['delay_id' => 'id']],
            [['wedding_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wedding::className(), 'targetAttribute' => ['wedding_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'lead' => 'Lead',
            'imageFile' => 'Image',
            'street_house' => 'Street House',
            'city' => 'City',
            'animate_id' => 'Animate ID',
            'delay_id' => 'Delay ID',
            'wedding_id' => 'Wedding ID',
        ];
    }

    /**
     * Upload and resizing image
     */
    public function uploadImage()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'uploads/wedding/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteImageFile();
                $image = new Image();
                $thumbPath = 'uploads/wedding/images/place-' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 681, 685, 80)) {
                    $this->image = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     */
    protected function deleteImageFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->image))) {
            unlink(Yii::getAlias('@frontend/web' . $this->image));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnimate()
    {
        return $this->hasOne(Animate::className(), ['id' => 'animate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelay()
    {
        return $this->hasOne(Delay::className(), ['id' => 'delay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWedding()
    {
        return $this->hasOne(Wedding::className(), ['id' => 'wedding_id']);
    }
}
