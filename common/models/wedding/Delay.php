<?php

namespace common\models\wedding;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%delay}}".
 *
 * @property integer $id
 * @property string $value
 *
 * @property Hotel[] $hotels
 * @property Person[] $people
 * @property Place[] $places
 * @property Story[] $stories
 * @property Story[] $stories0
 */
class Delay extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delay}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 4],
            [['value'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['delay_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['delay_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces()
    {
        return $this->hasMany(Place::className(), ['delay_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories()
    {
        return $this->hasMany(Story::className(), ['img_delay_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories0()
    {
        return $this->hasMany(Story::className(), ['txt_delay_id' => 'id']);
    }

    /**
     * @return array array of the values of the delays
     */
    public static function getAll()
    {
        return self::find()->select(['value', 'id'])->indexBy('id')->column();
    }
}
