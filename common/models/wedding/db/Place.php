<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Place
 * @package common\models\wedding\db
 */
class Place extends Model
{
    /**
     * @return array
     */
    public static function findAll()
    {
        $places = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'p.title, ' .
                'p.lead, ' .
                'p.street_house, ' .
                'p.city, ' .
                'p.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM place AS p ' .
                'LEFT JOIN wedding AS w ON p.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON p.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON p.delay_id=d.id ' .
                'WHERE w.status=:status'
            )
            ->bindValue(':status', Wedding::STATUS_ACTIVE)
            ->queryAll();

        return $places;
    }
}
