<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Person
 * @package common\models\wedding\db
 */
class Person extends Model
{
    /**
     * @return array|false
     */
    public static function findGroom()
    {
        $result = [];
        $groom = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'p.name, ' .
                'p.surname, ' .
                'p.text, ' .
                'p.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM person AS p ' .
                'LEFT JOIN wedding AS w ON p.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON p.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON p.delay_id=d.id ' .
                'LEFT JOIN status AS s ON p.status_id=s.id ' .
                'WHERE w.status=:status AND s.name=:status_name'
            )
            ->bindValues([
                ':status' => Wedding::STATUS_ACTIVE,
                ':status_name' => 'groom',
            ])
            ->queryOne();

        return $groom ? $result[] = $groom : $result;
    }

    /**
     * @return array|false
     */
    public static function findBride()
    {
        $result = [];
        $bride = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'p.name, ' .
                'p.surname, ' .
                'p.text, ' .
                'p.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM person AS p ' .
                'LEFT JOIN wedding AS w ON p.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON p.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON p.delay_id=d.id ' .
                'LEFT JOIN status AS s ON p.status_id=s.id ' .
                'WHERE w.status=:status AND s.name=:status_name'
            )
            ->bindValues([
                ':status' => Wedding::STATUS_ACTIVE,
                ':status_name' => 'bride',
            ])
            ->queryOne();

        return $bride ? $result[] = $bride : $result;
    }

    /**
     * @return array
     */
    public static function findBuddies()
    {
        $buddies = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'p.name, ' .
                'p.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM person AS p ' .
                'LEFT JOIN wedding AS w ON p.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON p.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON p.delay_id=d.id ' .
                'LEFT JOIN status AS s ON p.status_id=s.id ' .
                'WHERE w.status=:status AND s.name=:status_name'
            )
            ->bindValues([
                ':status' => Wedding::STATUS_ACTIVE,
                ':status_name' => 'groomsmen',
            ])
            ->queryAll();

        return $buddies;
    }

    /**
     * @return array
     */
    public static function findBridesmaids()
    {
        $bridesmaids = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'p.name, ' .
                'p.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM person AS p ' .
                'LEFT JOIN wedding AS w ON p.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON p.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON p.delay_id=d.id ' .
                'LEFT JOIN status AS s ON p.status_id=s.id ' .
                'WHERE w.status=:status AND s.name=:status_name'
            )
            ->bindValues([
                ':status' => Wedding::STATUS_ACTIVE,
                ':status_name' => 'bridesmaid',
            ])
            ->queryAll();

        return $bridesmaids;
    }
}
