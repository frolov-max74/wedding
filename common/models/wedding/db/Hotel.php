<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Hotel
 * @package common\models\wedding\db
 */
class Hotel extends Model
{
    /**
     * @return array
     */
    public static function findAll()
    {
        $hotels = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'h.title, ' .
                'h.street_house, ' .
                'h.city, ' .
                'h.image, ' .
                'a.name AS animate_name, ' .
                'd.value AS delay_value ' .
                'FROM hotel AS h ' .
                'LEFT JOIN wedding AS w ON h.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON h.animate_id=a.id ' .
                'LEFT JOIN delay AS d ON h.delay_id=d.id ' .
                'WHERE w.status=:status'
            )
            ->bindValue(':status', Wedding::STATUS_ACTIVE)
            ->queryAll();

        return $hotels;
    }
}
