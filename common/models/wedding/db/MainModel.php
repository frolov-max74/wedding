<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Wedding
 * @package common\models\wedding\db
 */
class MainModel extends Model
{
    /**
     * @return array|false
     */
    public static function findActive()
    {
        $result = [];
        $wedding = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'title, ' .
                'groom_bride, ' .
                'story, ' .
                'details, ' .
                'attending, ' .
                'accommodation_top, ' .
                'accommodation_down, ' .
                'gallery, ' .
                'rsvp, ' .
                'bgs.first_bg, ' .
                'bgs.second_bg, ' .
                'bgs.third_bg, ' .
                'bgs.fourth_bg ' .
                'FROM wedding ' .
                'LEFT JOIN empty_bgs AS bgs ON bgs.wedding_id=wedding.id ' .
                'WHERE status=:status'
            )
            ->bindValue(':status', Wedding::STATUS_ACTIVE)
            ->queryOne();

        return $wedding ? $result[] = $wedding : $result;
    }
}
