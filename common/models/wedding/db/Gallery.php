<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Gallery
 * @package common\models\wedding\db
 */
class Gallery extends Model
{
    /**
     * @return array
     */
    public static function findAll()
    {
        $gallery = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                'image ' .
                'FROM gallery AS g ' .
                'LEFT JOIN wedding AS w ON g.wedding_id=w.id ' .
                'WHERE w.status=:status'
            )
            ->bindValue(':status', Wedding::STATUS_ACTIVE)
            ->queryAll();

        return $gallery;
    }
}
