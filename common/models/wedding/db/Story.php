<?php

namespace common\models\wedding\db;

use Yii;
use yii\base\Model;
use common\models\wedding\Wedding;

/**
 * Class Story
 * @package common\models\wedding\db
 */
class Story extends Model
{
    /**
     * @return array
     */
    public static function findAll()
    {
        $story = Yii::$app->db
            ->createCommand(
                'SELECT ' .
                's.title, ' .
                's.text, ' .
                's.date, ' .
                's.image, ' .
                'a.name AS img_animate_name, ' .
                'an.name AS txt_animate_name, ' .
                'd.value AS img_delay_value, ' .
                'del.value AS txt_delay_value ' .
                'FROM stories AS s ' .
                'LEFT JOIN wedding AS w ON s.wedding_id=w.id ' .
                'LEFT JOIN animate AS a ON s.img_animate_id=a.id ' .
                'LEFT JOIN animate AS an ON s.txt_animate_id=an.id ' .
                'LEFT JOIN delay AS d ON s.img_delay_id=d.id ' .
                'LEFT JOIN delay AS del ON s.txt_delay_id=del.id ' .
                'WHERE w.status=:status'
            )
            ->bindValue(':status', Wedding::STATUS_ACTIVE)
            ->queryAll();

        return $story;
    }
}
