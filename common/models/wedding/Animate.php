<?php

namespace common\models\wedding;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%animate}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Hotel[] $hotels
 * @property Person[] $people
 * @property Place[] $places
 * @property Story[] $stories
 * @property Story[] $stories0
 */
class Animate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%animate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 12],
            [['name'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['animate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['animate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces()
    {
        return $this->hasMany(Place::className(), ['animate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories()
    {
        return $this->hasMany(Story::className(), ['img_animate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories0()
    {
        return $this->hasMany(Story::className(), ['txt_animate_id' => 'id']);
    }

    /**
     * @return array array of the names of the animations
     */
    public static function getAll()
    {
        return self::find()->select(['name', 'id'])->indexBy('id')->column();
    }
}
