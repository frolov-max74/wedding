<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\components\Image;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $avatar
 * @property string $website
 * @property integer $user_id
 *
 * @property User $user
 */
class Profile extends ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['first_name', 'last_name', 'website'], 'string', 'max' => 32],
            [['imageFile'], 'file', 'maxSize' => 1024 * 1024, 'extensions' => 'png, jpg, jpeg, gif', 'checkExtensionByMimeType' => false],
            [['first_name', 'last_name', 'website'], 'trim'],
            [['avatar'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'imageFile' => 'Avatar',
            'website' => 'Website',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return array|Profile|null|ActiveRecord
     */
    public static function findAuthUserProfile()
    {
        $id = User::getAuthUserId();
        $profile = self::find()->where(['user_id' => $id])->one();
        if (!$profile) {
            $profile = new self();
            $profile->user_id = $id;
        }
        return $profile;
    }

    /**
     * Upload and resizing avatar
     */
    public function uploadAvatar()
    {
        $basePath = Yii::getAlias('@frontend/web/');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $srcPath = $basePath . 'uploads/avatars/' . $this->user_id . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs($srcPath)) {
                $this->deleteAvatarFile();
                $image = new Image();
                $thumbPath = 'uploads/avatars/thumb-' . $this->user_id . '.' . $this->imageFile->extension;
                $destPath = $basePath . $thumbPath;
                if ($image->resizeImage($srcPath, $destPath, 128, 128, 80)) {
                    $this->avatar = '/' . $thumbPath;
                    unlink($srcPath);
                }
            }
        }
    }

    /**
     * Before deleting a record deletes the avatar file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteAvatarFile();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the avatar file
     */
    protected function deleteAvatarFile()
    {
        if (is_file(Yii::getAlias('@frontend/web' . $this->avatar))) {
            unlink(Yii::getAlias('@frontend/web' . $this->avatar));
        }
    }

    /**
     * @return string full name
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
