<?php

namespace common\rbac\rules;

use yii\rbac\Rule;

/**
 * Checks that the user cannot delete itself
 *
 * Class DeleteUserRule
 * @package common\rbac\rules
 */
class DeleteUserRule extends Rule
{
    /**
     * @var string rule name
     */
    public $name = 'deleteUser';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        return isset($params['id']) ? $params['id'] != $user : false;
    }
}
