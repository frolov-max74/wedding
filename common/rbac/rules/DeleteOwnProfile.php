<?php

namespace common\rbac\rules;

/**
 * It rule verifies that the user can delete their own profile
 *
 * Class DeleteOwnProfile
 * @package common\rbac\rules
 */
class DeleteOwnProfile extends UpdateOwnProfile
{

}
