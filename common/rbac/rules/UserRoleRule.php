<?php

namespace common\rbac\rules;

use Yii;
use yii\rbac\Rule;
use common\models\User;

/**
 * Check the user activity
 *
 * Class UserRoleRule
 * @package common\rbac\rules
 */
class  UserRoleRule extends Rule
{
    /**
     * @var string rule name
     */
    public $name = 'isUserActive';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            return User::STATUS_ACTIVE == Yii::$app->user->identity->status;
        }
        return false;
    }
}
