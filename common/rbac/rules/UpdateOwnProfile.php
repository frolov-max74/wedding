<?php

namespace common\rbac\rules;

use yii\rbac\Rule;

/**
 * It rule verifies that the user can update their own profile
 *
 * Class UpdateOwnProfile
 * @package common\rbac\rules
 */
class UpdateOwnProfile extends Rule
{
    /**
     * @var string rule name
     */
    public $name = 'isOwnProfile';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        return isset($params['profile']['user_id']) ? $params['profile']['user_id'] == $user : false;
    }
}
