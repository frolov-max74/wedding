<?php
return [
    'dashboard' => [
        'type' => 2,
        'description' => 'Dashboard',
    ],
    'deleteUser' => [
        'type' => 2,
        'description' => 'Delete user',
        'ruleName' => 'deleteUser',
    ],
    'updateOwnProfile' => [
        'type' => 2,
        'description' => 'Update own profile',
        'ruleName' => 'isOwnProfile',
    ],
    'deleteOwnProfile' => [
        'type' => 2,
        'description' => 'Delete own profile',
        'ruleName' => 'isOwnProfile',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
        'ruleName' => 'isUserActive',
    ],
    'owner' => [
        'type' => 1,
        'description' => 'Owner',
        'ruleName' => 'isUserActive',
        'children' => [
            'dashboard',
            'updateOwnProfile',
            'deleteOwnProfile',
            'user',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Administrator',
        'ruleName' => 'isUserActive',
        'children' => [
            'deleteUser',
            'owner',
        ],
    ],
];
