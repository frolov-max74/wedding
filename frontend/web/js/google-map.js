function init_map() {

    var myOptions = {
        zoom: 16,
        center: new google.maps.LatLng(56.8278359, 60.5917025),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('gmap-canvas'), myOptions);

    marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(56.8278359, 60.5917025)
    });

    infowindow = new google.maps.InfoWindow({
        content: '<strong>ЗАГС Ленинского района</strong><br>Екатеринбург, ул.Сакко и Ванцетти, 105/1<br>'
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });

    infowindow.open(map, marker);
}

google.maps.event.addDomListener(window, 'load', init_map);
