<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class WeddingAsset
 * @package frontend\assets
 */
class WeddingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
    ];
    public $js = [
        'js/jquery.easing.min.js',
        'js/jquery.stellar.min.js',
        'js/jquery.sticky.js',
        'js/jquery.countdown.min.js',
        'js/template-custom.js',
        'js/google-map.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\IoniconsAsset',
        'frontend\assets\AnimateAsset',
        'frontend\assets\FlexsliderAsset',
        'frontend\assets\WowAsset',
        'frontend\assets\LightboxAsset',
        'frontend\assets\IE8SupportAsset',
        'frontend\assets\GoogleMapAsset',
    ];
}
