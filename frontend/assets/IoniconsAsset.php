<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class IoniconsAsset
 * @package frontend\assets
 */
class IoniconsAsset extends AssetBundle
{
    public $sourcePath = '@bower/ionicons';
    public $css = [
        'css/ionicons.min.css',
    ];
}
