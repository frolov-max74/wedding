<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class GoogleMapAsset
 * @package frontend\assets
 */
class GoogleMapAsset extends AssetBundle
{
    public $js = [
        //'https://maps.googleapis.com/maps/api/js?v=3.exp',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyCRmYPVG11mAGdEtrzilSl7Ie6WAExdXmA',
    ];
}
