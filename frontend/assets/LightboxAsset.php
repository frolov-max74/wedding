<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class LightboxAsset
 * @package frontend\assets
 */
class LightboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/lightbox2';
    public $css = [
        'dist/css/lightbox.min.css',
    ];
    public $js = [
        'dist/js/lightbox.min.js',
    ];
}
