<?php

namespace frontend\assets;

use yii\web\{
    AssetBundle,
    View
};

/**
 * Class IE8SupportAsset for IE8 support asset bundle.
 * @package frontend\assets
 */
class IE8SupportAsset extends AssetBundle
{
    public $js = [
        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
    ];
    public $jsOptions = [
        'condition' => 'lt IE9',
        'position' => View::POS_HEAD,
    ];
}
