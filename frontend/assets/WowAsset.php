<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class WowAsset
 * @package frontend\assets
 */
class WowAsset extends AssetBundle
{
    public $sourcePath = '@bower/wow';
    public $js = [
        'dist/wow.min.js',
    ];
}
