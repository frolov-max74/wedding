<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FlexsliderAsset
 * @package frontend\assets
 */
class FlexsliderAsset extends AssetBundle
{
    public $sourcePath = '@bower/flexslider';
    public $css = [
        'flexslider.css',
    ];
    public $js = [
        'jquery.flexslider.js',
    ];
}
