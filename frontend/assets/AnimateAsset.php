<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AnimateAsset
 * @package frontend\assets
 */
class AnimateAsset extends AssetBundle
{
    public $sourcePath = '@bower/animate.css';
    public $css = [
        'animate.min.css',
    ];
}
