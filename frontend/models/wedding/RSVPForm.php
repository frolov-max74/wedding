<?php

namespace frontend\models\wedding;

use Yii;
use common\models\wedding\Guest;
use yii\base\Model;

/**
 * Class RSVPForm
 * @package frontend\models\wedding RSVPForm is the model behind the rsvp form.
 */
class RSVPForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $gender;
    public $presence;
    public $message;
    public $created_at;
    public $verifyCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'gender', 'presence', 'verifyCode'], 'required'],
            [['name', 'surname', 'email', 'message'], 'trim'],
            [['name', 'surname'], 'string', 'min' => 2, 'max' => 32],
            [['gender', 'presence'], 'string', 'max' => 1],
            [['message'], 'string'],
            [['message'], 'default', 'value' => ''],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 128],
            [
                ['email'],
                'unique',
                'targetClass' => '\common\models\wedding\Guest',
                'message' => 'Этот email уже используется.'
            ],
            [['created_at'], 'integer'],
            [['created_at'], 'default', 'value' => function () {
                return date('U');
            }],
            [['created_at'], function ($attribute) {
                if ($this->$attribute >= Yii::$app->params['datetimeWedding']) {
                    $this->addError($attribute, 'Регистрация уже невозможна.');
                }
            }],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'captchaAction' => 'wedding/captcha'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'Email',
            'gender' => 'Пол',
            'presence' => 'Ваше присутствие',
            'message' => 'Ваше сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Guest registration
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        $guest = new Guest();
        $guest->name = $this->name;
        $guest->surname = $this->surname;
        $guest->email = $this->email;
        $guest->gender = $this->gender;
        $guest->presence = $this->presence;
        $guest->message = $this->message;
        $guest->created_at = $this->created_at;

        return $guest->save(false) ? true : false;
    }

}
