<?php

/* @var $urlImage string */
/* @var $message string */

?>
<div class="empty-image parallax-background" style=" background-image: url(<?= $urlImage ?>);"
     data-stellar-background-ratio="0.3">
    <?php if ($message): ?>
        <?= $message ?>
    <?php endif; ?>
</div>
