<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class EmptyImageBlock
 * @package frontend\widgets
 */
class EmptyImageBlock extends Widget
{
    /**
     * @var string the image url is relative to the directory webroot
     */
    public $urlImage;

    /**
     * @var null|string message
     */
    public $message;

    public function init()
    {
        parent::init();

        $pathImage = '@webroot' . $this->urlImage;

        if (null === $this->urlImage) {
            $this->message = 'No image';
        } elseif ('' === $this->urlImage) {
            $this->message = 'No image URL';
        } elseif (!is_file(Yii::getAlias($pathImage))) {
            $this->message = 'No such image: ' . $this->urlImage;
        }
    }

    public function run()
    {
        parent::run();

        return $this->render('divEmptyImage', [
            'urlImage' => Html::encode($this->urlImage),
            'message' => $this->message,
        ]);
    }
}
