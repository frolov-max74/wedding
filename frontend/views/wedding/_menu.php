<?php

use yii\helpers\Html;

?>
<header class="navbar navbar-default transparent navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <div class="logo-rouded">
                    <strong>Wedding</strong>
                    <i class="ion-heart"></i>
                </div>
            </a><!--logo for mobile and tablet-->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right scroll-to">
                <li class="active"><?= Html::a('Home', '#home') ?></li>
                <li><?= Html::a('История', '#story') ?></li>
                <li><?= Html::a('Свадьба', '#wedding') ?></li>
                <li><?= Html::a('Друзья', '#grmen') ?></li>
                <li><?= Html::a('Подруги', '#brmaid') ?></li>
                <li><?= Html::a('Галерея', '#gallery') ?></li>
                <li><?= Html::a('rsvp', '#rsvp') ?></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container -->
</header><!--header end-->
