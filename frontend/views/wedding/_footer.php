<div class="thanks" data-stellar-background-ratio="0.3">
    <div class="container">
        <span>
            &copy; Copyright 2017. <a href="<?= Yii::$app->params['baseUrl'] ?>"><?= Yii::$app->name ?></a>.
        </span>
    </div>
</div>
