<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use common\models\wedding\GendersList;
use common\models\wedding\EventsList;
use yii\bootstrap\ActiveForm;
use frontend\widgets\EmptyImageBlock;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $rsvp frontend\models\wedding\RSVPForm */
/* @var $wedding array */

$this->title = Yii::$app->name;
?>
<section id="home" class="hero-fullscreen hero_one parallax-background" data-stellar-background-ratio="0.3">
    <div class="hero-overlay"></div>
    <div class="hero-table">
        <div class="hero-vm">
            <div class="container text-center">
                <h1><?= $wedding['groom']['name'] ?> <span>&</span> <?= $wedding['bride']['name'] ?></h1>
                <p><?= $wedding['title'] ?></p>
            </div>
        </div>
    </div><!--hero full screen-->
</section>
<!--intro section end-->
<div class="space-100"></div>

<section class="groom-bride">
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title wow animated bounceIn" data-wow-delay="0.1s">
                    <h2>Вы приглашены</h2>
                    <p class="lead text-color"><?= $wedding['title'] ?></p>
                    <p><?= $wedding['groom_bride'] ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-6 text-center margin-b-30 wow animated <?= $wedding['groom']['animate_name'] ?>"
                         data-wow-delay="<?= $wedding['groom']['delay_value'] ?>">
                        <div class="gb_box">
                            <div class="image-box">
                                <?= Html::img(Url::to($wedding['groom']['image']), [
                                    'class' => 'img-responsive',
                                    'alt' => 'groom',
                                ]) ?>
                            </div>
                            <div class="space-20"></div>
                            <h4><?= $wedding['groom']['full_name'] ?></h4>
                            <p><?= $wedding['groom']['text'] ?></p>
                        </div>
                    </div><!--col 6 end-->
                    <div class="col-sm-6 text-center margin-b-30 wow animated <?= $wedding['bride']['animate_name'] ?>"
                         data-wow-delay="<?= $wedding['bride']['delay_value'] ?>">
                        <div class="gb_box">
                            <div class="image-box">
                                <?= Html::img(Url::to($wedding['bride']['image']), [
                                    'class' => 'img-responsive',
                                    'alt' => 'bride',
                                ]) ?>
                            </div>
                            <div class="space-20"></div>
                            <h4><?= $wedding['bride']['full_name'] ?></h4>
                            <p><?= $wedding['bride']['text'] ?></p>
                        </div>
                    </div><!--col 6 end-->
                </div><!--row end-->
            </div>
        </div>
    </div><!--container end-->
</section>
<!--groom-bride end-->
<div class="space-70"></div>

<?= EmptyImageBlock::widget(['urlImage' => $wedding['first_bg']]) ?>

<section id="story" class="our-story">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Наша <i class="ion-heart"></i> история</h2>
                    <p><?= $wedding['story_text'] ?></p>
                </div>
            </div>
        </div>
        <?php $i = 1;
        foreach ($wedding['story'] as $item): ?>
            <?php if (($i % 2) !== 0): ?>
                <div class="row story-row">
                    <div class="col-sm-12 col-md-offset-1 col-md-4 text-center wow animated <?= $item['img_animate_name'] ?>"
                         data-wow-delay="<?= $item['img_delay_value'] ?>">
                        <div class="image-box">
                            <?= Html::img(Url::to($item['image']), [
                                'class' => 'img-responsive',
                                'alt' => '',
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2 text-center">
                        <div class="story-date">
                            <div class="date-only"><?= $item['day'] ?></div>
                            <div class="month-year"><?= $item['month_year'] ?></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 wow animated <?= $item['txt_animate_name'] ?>"
                         data-wow-delay="<?= $item['txt_delay_value'] ?>">
                        <h3><span><?= $i ?>.</span> <?= $item['title'] ?></h3>
                        <p><?= $item['text'] ?></p>
                    </div>
                    <div class="vertical-line"></div>
                </div><!--story row-->
            <?php else: ?>
                <div class="row story-row">
                    <div class="col-sm-12 col-md-offset-1 col-md-4 text-left wow animated <?= $item['txt_animate_name'] ?>"
                         data-wow-delay="<?= $item['txt_delay_value'] ?>">
                        <h3><span><?= $i ?>.</span> <?= $item['title'] ?></h3>
                        <p><?= $item['text'] ?></p>
                    </div>
                    <div class="col-sm-12 col-md-2 text-center">
                        <div class="story-date">
                            <div class="date-only"><?= $item['day'] ?></div>
                            <div class="month-year"><?= $item['month_year'] ?></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 text-center wow animated <?= $item['img_animate_name'] ?>"
                         data-wow-delay="<?= $item['img_delay_value'] ?>">
                        <div class="image-box">
                            <?= Html::img(Url::to($item['image']), [
                                'class' => 'img-responsive',
                                'alt' => '',
                            ]) ?>
                        </div>
                    </div>
                    <div class="vertical-line"></div>
                </div><!--story row-->
            <?php endif; ?>
            <?php $i++;
        endforeach; ?>
    </div>
</section>
<!--end our story-->
<div class="space-100"></div>

<div class="parallax-background get-married" data-stellar-background-ratio="0.3">
    <div class="container text-center">
        <h1>И теперь... Мы решили пожениться</h1>
        <p><?= $wedding['title'] ?></p>
        <div class="space-50"></div>
        <div id="getting-married"></div>
    </div>
</div>

<section id="wedding">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Свадебные <i class="ion-heart"></i> Подробности</h2>
                    <p><?= $wedding['details'] ?></p>
                </div>
                <hr>
            </div>
        </div>
        <div class="row wedding-details text-center">
            <?php $i = 1;
            foreach ($wedding['places'] as $place): ?>
                <div class="col-sm-5 <?= ($i % 2) !== 0 ? 'col-sm-offset-1' : '' ?> text-center margin-b-50 wow animated <?= $place['animate_name'] ?>"
                     data-wow-delay="<?= $place['delay_value'] ?>">
                    <h3><?= $place['title'] ?></h3>
                    <p><?= $place['lead'] ?></p>
                    <div class="space-20"></div>
                    <div class="image-box">
                        <?= Html::img(Url::to($place['image']), [
                            'class' => 'img-responsive',
                            'alt' => '',
                        ]) ?>
                    </div>
                    <div class="space-20"></div>
                    <p>
                        <?= $place['street_house'] ?><br>
                        <?= $place['city'] ?>
                    </p>
                </div>
                <?php $i++;
            endforeach; ?>
        </div>
        <hr>
        <div class="space-50"></div>
        <div class="row margin-b-30 scroll-to">
            <div class="col-sm-6 col-sm-offset-3 text-center wow animated fadeInUp" data-wow-delay="0.4s">
                <h2 class="text-uppercase">Вы готовы прийти?</h2>
                <p class="margin-b-20"><?= $wedding['attending'] ?></p>
                <a href="#rsvp" class="btn btn-dark-border btn-lg">Rsvp</a>
            </div>
        </div>
    </div>
</section>
<div class="space-50"></div>

<?= EmptyImageBlock::widget(['urlImage' => $wedding['second_bg']]) ?>
<div class="space-100"></div>

<div class="overflow-hidden">
    <div class="container ">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Размещение</h2>
                    <p><?= $wedding['hotel_top'] ?></p>
                </div>
            </div>
        </div>
        <div class="row accomodations-row">
            <?php foreach ($wedding['hotels'] as $hotel): ?>
                <div class="col-sm-4 margin-b-30 wow animated <?= $hotel['animate_name'] ?>"
                     data-wow-delay="<?= $hotel['delay_value'] ?>">
                    <div class="image-box">
                        <?= Html::img(Url::to($hotel['image']), [
                            'class' => 'img-responsive',
                            'alt' => '',
                        ]) ?>
                    </div>
                    <h4><?= $hotel['title'] ?></h4>
                    <p>
                        <?= $hotel['street_house'] ?><br>
                        <?= $hotel['city'] ?>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
        <hr>
        <div class="text-center">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <p><?= $wedding['hotel_down'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="grmen" class="persons">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Друзья Жениха</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <?php foreach ($wedding['buddies'] as $buddy): ?>
                        <div class="col-sm-4 margin-b-30 text-center wow animated <?= $buddy['animate_name'] ?>"
                             data-wow-delay="<?= $buddy['delay_value'] ?>">
                            <?= Html::img(Url::to($buddy['image']), [
                                'width' => 200,
                                'class' => 'img-responsive img-circle',
                                'alt' => 'groomsmen',
                            ]) ?>
                            <div class="space-20"></div>
                            <h4><?= $buddy['name'] ?></h4>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end groomsmen-->
<div class="space-70"></div>

<?= EmptyImageBlock::widget(['urlImage' => $wedding['third_bg']]) ?>

<section id="brmaid" class="persons">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Подруги Невесты</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <?php foreach ($wedding['bridesmaids'] as $bridesmaid): ?>
                        <div class="col-sm-4 margin-b-30 text-center wow animated <?= $bridesmaid['animate_name'] ?>"
                             data-wow-delay="<?= $bridesmaid['delay_value'] ?>">
                            <?= Html::img(Url::to($bridesmaid['image']), [
                                'width' => 200,
                                'class' => 'img-responsive img-circle',
                                'alt' => 'bridesmaid',
                            ]) ?>
                            <div class="space-20"></div>
                            <h4><?= $bridesmaid['name'] ?></h4>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="space-70"></div>
</section>
<!--end bridesmaid-->
<?= EmptyImageBlock::widget(['urlImage' => $wedding['fourth_bg']]) ?>

<section id="gallery">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Галерея</h2>
                    <p><?= $wedding['gallery_text'] ?></p>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <?php foreach ($wedding['gallery'] as $item): ?>
                <div class="col-sm-3 no-padd">
                    <?= Html::a(
                        Html::img(
                            Url::to($item['image']),
                            ['class' => 'img-responsive', 'alt' => '']
                        ),
                        Url::to($item['image']),
                        ['class' => 'image-box', 'data-lightbox' => 'gallery',]
                    ) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!--end gallery-->

<section id="rsvp">
    <div class="space-100"></div>
    <div class="container">
        <div class="row margin-b-50">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="center-title">
                    <h2>Rsvp</h2>
                    <p><?= $wedding['rsvp'] ?></p>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">

                <?= Alert::widget() ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'form-rsvp',
                ]); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($rsvp, 'name')->textInput([
                            'id' => 'name',
                            'class' => 'form-control',
                        ]) ?>

                        <?= $form->field($rsvp, 'surname')->textInput([
                            'id' => 'surname',
                            'class' => 'form-control',
                        ]) ?>

                        <?= $form->field($rsvp, 'email')->input('email', [
                            'id' => 'email',
                            'class' => 'form-control',
                        ]) ?>

                        <?= $form->field($rsvp, 'gender')->dropDownList(GendersList::$names, [
                            'prompt' => 'Выбрать',
                            'id' => 'gender',
                        ]) ?>

                        <?= $form->field($rsvp, 'presence')->dropDownList(EventsList::$names, [
                            'prompt' => 'Выбрать',
                            'id' => 'presence',
                        ]) ?>

                        <?= $form->field($rsvp, 'created_at')->hiddenInput()->label(false) ?>

                        <?= $form->field($rsvp, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-8">{input}</div></div>',
                            'captchaAction' => 'wedding/captcha',
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($rsvp, 'message')->textarea([
                            'id' => 'message',
                            'rows' => '17',
                        ]) ?>

                        <?= Html::submitButton('Отправить', [
                            'class' => 'btn btn-skin btn-lg btn-block',
                        ]) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="space-100"></div>
</section>
<!--end rsvp-->

<div id='gmap-canvas'></div>
<!--end google-map-->
