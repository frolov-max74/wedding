<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\{
    Html,
    Url
};
use frontend\assets\WeddingAsset;

WeddingAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/ico', 'href' => Url::to(['/favicon-heart.ico'])]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body data-spy="scroll">
<?php $this->beginBody() ?>
<div id="preloader"></div>
<?= $this->render('//wedding/_menu') ?>

<?= $content ?>

<?= $this->render('//wedding/_footer') ?>
<?= Html::a('<i class="ion-android-arrow-dropup-circle"></i>', '#', ['class' => 'scrollToTop']) ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
