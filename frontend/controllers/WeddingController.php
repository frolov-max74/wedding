<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\wedding\Wedding;
use frontend\models\wedding\RSVPForm;
use common\models\wedding\cards\CardWedding;
use common\models\wedding\db\MainModel;

class WeddingController extends Controller
{
    public $layout = 'wedding';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xEF3030,
                'minLength' => 6,
                'maxLength' => 6,
                'offset' => 4,
            ],
        ];
    }

    public function actionIndex()
    {
        if (!Wedding::findActive()) {
            return $this->redirect('/site/error');
        }

        $rsvp = new RSVPForm();
        $wedding = MainModel::findActive();

        if ($rsvp->load(Yii::$app->request->post())) {
            if ($rsvp->register()) {
                Yii::$app->session->setFlash('success', 'Спасибо! Вы успешно зарегистрированы.');
            } elseif ($rsvp->hasErrors()) {
                Yii::$app->session->setFlash('error', $rsvp->getFirstErrors());
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка. Повторите попытку позже.');
            }
            return $this->refresh('#rsvp');
        }

        return $this->render('index', [
            'rsvp' => $rsvp,
            'wedding' => CardWedding::findFull($wedding),
        ]);
    }
}
