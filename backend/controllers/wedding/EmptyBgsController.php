<?php

namespace backend\controllers\wedding;

use Yii;
use common\models\wedding\EmptyBgs;
use common\models\wedding\Wedding;
use backend\models\wedding\search\EmptyBgsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * EmptyBgsController implements the CRUD actions for EmptyBgs model.
 */
class EmptyBgsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('owner')) {
                throw new ForbiddenHttpException('You can\'t perform this action');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all EmptyBgs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmptyBgsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'weddingIdsList' => Wedding::findAllIds(),
        ]);
    }

    /**
     * Displays a single EmptyBgs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmptyBgs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmptyBgs();

        if ($model->load(Yii::$app->request->post())) {
            $model->uploadImages();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изображения успешно сохранены.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'weddingIdsList' => Wedding::findAllIds(),

            ]);
        }
    }

    /**
     * Updates an existing EmptyBgs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->uploadImages();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изображения успешно сохранены.');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'weddingIdsList' => Wedding::findAllIds(),

            ]);
        }
    }

    /**
     * Deletes an existing EmptyBgs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmptyBgs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmptyBgs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmptyBgs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
