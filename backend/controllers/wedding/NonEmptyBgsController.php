<?php

namespace backend\controllers\wedding;

use Yii;
use yii\web\Controller;
use common\models\wedding\NonEmptyBgs;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

class NonEmptyBgsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!\Yii::$app->user->can('owner')) {
                throw new ForbiddenHttpException('You can\'t perform this action');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays all non-empty backgrounds.
     * @return string
     */
    public function actionIndex()
    {
        $model = new NonEmptyBgs();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Non-EmptyBgs model.
     * @return string
     */
    public function actionView()
    {
        $model = new NonEmptyBgs();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new NonEmptyBgs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new NonEmptyBgs();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->uploadImages()) {
                Yii::$app->session->setFlash('success', 'Изображения успешно сохранены.');
                return $this->redirect(['view']);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        } else {
            return $this->render('create', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Updates an existing EmptyBgs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = new NonEmptyBgs();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->uploadImages()) {
                Yii::$app->session->setFlash('success', 'Изображения успешно сохранены.');
                return $this->redirect(['view']);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrors());
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Deletes all non-empty backgrounds.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return string
     */
    public function actionDelete()
    {
        $model = new NonEmptyBgs();

        if (
            file_exists($model->homePath) ||
            file_exists($model->marriedPath) ||
            file_exists($model->thanksPath)
        ) {
            unlink($model->homePath);
            unlink($model->marriedPath);
            unlink($model->thanksPath);
        }

        return $this->redirect(['index']);
    }

}
