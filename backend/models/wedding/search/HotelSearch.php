<?php

namespace backend\models\wedding\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\wedding\Hotel;

/**
 * HotelSearch represents the model behind the search form about `common\models\wedding\Hotel`.
 */
class HotelSearch extends Hotel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wedding_id'], 'integer'],
            [['title', 'street_house', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hotel::find()->with('wedding');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'wedding_id' => $this->wedding_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'street_house', $this->street_house])
            ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }
}
