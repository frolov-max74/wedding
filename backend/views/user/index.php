<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'username',
                // 'auth_key',
                // 'password_hash',
                // 'password_reset_token',
                'email:email',
                [
                    'attribute' => 'status',
                    'label' => 'Active',
                    'format' => 'boolean',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status',
                        [10 => 'Да', 0 => 'Нет'],
                        ['prompt' => 'Выбрать']
                    ),
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'dd-MM-Y HH:mm:ss'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]),
                ],
                [
                    'attribute' => 'updated_at',
                    'format' => ['date', 'dd-MM-Y HH:mm:ss'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'updated_at',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]),
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

</div>
