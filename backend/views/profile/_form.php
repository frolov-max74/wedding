<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="profile-form">

    <?php $form = ActiveForm::begin([
        // 'enctype' optional starting with version 2.0.8, if the form has a field for the file upload
        // 'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $model->avatar ? Html::img(
        Yii::$app->params['baseUrl'] . $model->avatar,
        ['class' => 'img-circle img-responsive']
    ) : ''; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
