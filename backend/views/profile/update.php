<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */


if (!Yii::$app->user->can('user')) {
    $this->title = 'Update Profile: ' . $model->fullName;
    $this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->fullName, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = 'Update';
} else {
    $this->title = 'Profile: ' . $model->fullName;
    $this->params['breadcrumbs'][] = $model->fullName;
}
?>
<div class="profile-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
