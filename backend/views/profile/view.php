<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */

$this->title = 'Profile: ' . $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->fullName;
?>
<div class="profile-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот профиль?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->avatar,
                        [
                            'class' => 'img-circle img-responsive',
                            'style' => 'width:50px',
                        ]
                    );
                }
            ],
            'first_name',
            'last_name',
            [
                'attribute' => 'website',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        Html::encode($model->website),
                        Url::to('//' . $model->website)
                    );
                }
            ],
            // 'user_id',
        ],
    ]) ?>

</div>
