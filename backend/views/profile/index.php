<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'first_name',
                'last_name',
                [
                    'attribute' => 'website',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::a(
                            Html::encode($data->website),
                            Url::to('//' . $data->website)
                        );
                    }
                ],
                [
                    'attribute' => 'avatar',
                    'label' =>false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->avatar,
                            [
                                'class' => 'img-circle img-responsive',
                                'style' => 'width: 50px',
                            ]
                        );
                    }
                ],
                // 'user_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

</div>
