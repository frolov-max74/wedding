<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->user->can('dashboard') ? 'Admin Panel' : 'Personal Cabinet';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">Вы успешно авторизовались.</p>

        <p>
            Начните пользоваться
            <?= Yii::$app->user->can('dashboard') ? 'админ панелью' : 'персональным кабинетом'; ?>.
        </p>
        <p>Вы можете создать и обновлять свой пользовательский профиль (вверху справа).</p>
        <?php if (Yii::$app->user->can('dashboard')): ?>
            <p>Обратите внимание на меню слева. Вы можете видеть ссылки для доступа к разделам админ панели.</p>
        <?php endif; ?>
    </div>

</div>
