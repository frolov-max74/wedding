<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $identity common\models\User */
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">

            <?php if (!empty($identity->profile->avatar)): ?>
                <div class="pull-left image">
                    <?= Html::img(
                        Yii::$app->params['baseUrl'] . $identity->profile->avatar,
                        ['class' => 'img-circle', 'alt' => 'User Image',]
                    ) ?>
                </div>
            <?php else: ?>
                <div class="pull-left image">
                    <?= Html::img(
                        Yii::$app->params['baseUrl'] . '/images/noimg.png',
                        ['class' => 'img-circle', 'alt' => 'User Image',]
                    ) ?>
                </div>
            <?php endif; ?>

            <div class="pull-left info">

                <?php if (!empty($identity->profile->fullName)): ?>
                    <p><?= $identity->profile->fullName ?></p>
                    <?= Html::a('<i class="fa fa-circle text-success"></i> Online', Url::to('#')) ?>
                <?php else: ?>
                    <p><?= $identity->username ?></p>
                    <?= Html::a('<i class="fa fa-circle text-success"></i> Online', Url::to('#')) ?>
                <?php endif; ?>

            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php if (Yii::$app->user->can('dashboard')): ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        [
                            'label' => 'Wedding Manager',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Guests', 'icon' => 'file-text-o', 'url' => ['/wedding/guest']],
                                ['label' => 'Wedding (Тексты блоков)', 'icon' => 'file-text-o', 'url' => ['/wedding/main']],
                                ['label' => 'Persons', 'icon' => 'file-text-o', 'url' => ['/wedding/person']],
                                ['label' => 'Story', 'icon' => 'file-text-o', 'url' => ['/wedding/story']],
                                ['label' => 'Places', 'icon' => 'file-text-o', 'url' => ['/wedding/place']],
                                ['label' => 'Hotels', 'icon' => 'file-text-o', 'url' => ['/wedding/hotel']],
                                ['label' => 'Gallery', 'icon' => 'file-text-o', 'url' => ['/wedding/gallery']],
                                [
                                    'label' => 'Backgrounds',
                                    'icon' => 'share',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Empty Backgrounds', 'icon' => 'file-text-o', 'url' => ['/wedding/empty-bgs']],
                                        ['label' => 'Non-Empty Backgrounds', 'icon' => 'file-text-o', 'url' => ['/wedding/non-empty-bgs']],
                                    ],
                                ],
                                ['label' => 'Statuses (Роли на свадьбе)', 'icon' => 'file-text-o', 'url' => ['/wedding/status']],
                                ['label' => 'Delays (Задержки фото)', 'icon' => 'file-text-o', 'url' => ['/wedding/delay']],
                                ['label' => 'Animations (Анимации фото)', 'icon' => 'file-text-o', 'url' => ['/wedding/animate']],
                            ],
                        ],
                        [
                            'label' => 'User Manager',
                            'icon' => 'share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Users', 'icon' => 'file-text-o', 'url' => ['/user']],
                                ['label' => 'User Profiles', 'icon' => 'file-text-o', 'url' => ['/profile']],
                            ],
                        ],
                        //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                        //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ],
                ]
            ) ?>
        <?php endif; ?>

    </section>

</aside>
