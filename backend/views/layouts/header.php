<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $identity common\models\User */
?>
<header class="main-header">

    <?php if (Yii::$app->user->can('dashboard')): ?>
        <?= Html::a(
            '<span class="logo-mini">AP</span><span class="logo-lg">Admin Panel</span>',
            Yii::$app->homeUrl, ['class' => 'logo']
        ) ?>
    <?php else: ?>
        <?= Html::a(
            '<span class="logo-mini">PC</span><span class="logo-lg">Personal Cabinet</span>',
            Yii::$app->homeUrl, ['class' => 'logo']
        ) ?>
    <?php endif; ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <?php if (!empty($identity->profile->avatar)): ?>
                            <?= Html::img(
                                Yii::$app->params['baseUrl'] . $identity->profile->avatar,
                                ['class' => 'user-image', 'alt' => 'User Image',]
                            ) ?>
                        <?php else: ?>
                            <?= Html::img(
                                Yii::$app->params['baseUrl'] . '/images/noimage.png',
                                ['class' => 'user-image', 'alt' => 'User Image',]
                            ) ?>
                        <?php endif; ?>

                        <?php if (!empty($identity->profile->fullName)): ?>
                            <span class="hidden-xs"><?= $identity->profile->fullName ?></span>
                        <?php else: ?>
                            <span class="hidden-xs"><?= $identity->username ?></span>
                        <?php endif; ?>

                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">

                            <?php if (!empty($identity->profile->avatar)): ?>
                                <?= Html::img(
                                    Yii::$app->params['baseUrl'] . $identity->profile->avatar,
                                    ['class' => 'img-circle', 'alt' => 'User Image',]
                                ) ?>
                            <?php else: ?>
                                <?= Html::img(
                                    Yii::$app->params['baseUrl'] . '/images/noimage.png',
                                    ['class' => 'img-circle', 'alt' => 'User Image',]
                                ) ?>
                            <?php endif; ?>

                            <p>
                                <?php if (!empty($identity->profile->fullName)): ?>
                                    <?= $identity->profile->fullName ?>
                                <?php else: ?>
                                    <?= $identity->username ?>
                                <?php endif; ?>
                                <small>
                                    Зарегистрирован: <?= date('d-m-Y', $identity->created_at) ?>
                                </small>
                            </p>

                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    'Профиль',
                                    ['profile/auth-user-profile'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выйти',
                                    ['site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
