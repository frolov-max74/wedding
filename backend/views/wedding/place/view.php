<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Place */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'lead',
            'street_house',
            'city',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->image,
                        [
                            'class' => 'img-responsive',
                            'style' => 'width: 300px',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'animate_id',
                'label' => 'Animation',
                'value' => function ($model) {
                    return $model->animate->name;
                }
            ],
            [
                'attribute' => 'delay_id',
                'label' => 'Delay',
                'value' => function ($model) {
                    return $model->delay->value;
                }
            ],
            'wedding_id',
        ],
    ]) ?>

</div>
