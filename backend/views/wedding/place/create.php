<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Place */
/* @var $weddingIdsList array all Wedding ID */
/* @var $animations array all Animate names */
/* @var $delays array all Delay values */

$this->title = 'Create Place';
$this->params['breadcrumbs'][] = ['label' => 'Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-create">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
        'animations' => $animations,
        'delays' => $delays,
    ]) ?>

</div>
