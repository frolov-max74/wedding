<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Delay */

$this->title = 'Update Delay: ' . $model->value;
$this->params['breadcrumbs'][] = ['label' => 'Delays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->value, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="delay-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
