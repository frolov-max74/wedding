<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Delay */

$this->title = 'Create Delay';
$this->params['breadcrumbs'][] = ['label' => 'Delays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delay-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
