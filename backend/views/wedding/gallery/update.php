<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Gallery */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Update Image: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Image: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gallery-update">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
    ]) ?>

</div>
