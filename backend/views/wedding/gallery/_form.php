<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Gallery */
/* @var $form yii\widgets\ActiveForm */
/* @var $weddingIdsList array all Wedding ID */
?>

<div class="gallery-form">

    <?= Html::tag(
        'p',
        '<span>Изображение будет обрезано в размер: 681X685 px. Вес загружаемой фотографии не должен превышать 2Mb!</span>>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Во избежание некорректной обрезки, загружайте изображение размером кратным данному размеру.</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Пожалуйста, заранее подготавливайте материал для публикации.</span>'
    ) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $model->image ? Html::img(
        Yii::$app->params['baseUrl'] . $model->image,
        ['class' => 'img-responsive']
    ) : ''; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form
        ->field($model, 'wedding_id')
        ->dropDownList(
            $weddingIdsList,
            [
                'prompt' => 'Select',
            ]
        ) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
