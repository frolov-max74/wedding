<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Gallery */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Create Image';
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
    ]) ?>

</div>
