<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Person */
/* @var $form yii\widgets\ActiveForm */
/* @var $statuses array all Status names */
/* @var $animations array all Animate names */
/* @var $delays array all Delay values */
/* @var $weddingIdsList array all Wedding ID */
?>

<div class="person-form">

    <?= Html::tag(
        'p',
        '<span>Изображение будет обрезано в размер: 500X500 px. Вес загружаемой фотографии не должен превышать 2Mb!</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Во избежание некорректной обрезки, загружайте изображение размером кратным данному размеру.</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Пожалуйста, заранее подготавливайте материал для публикации.</span>'
    ) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form
        ->field($model, 'surname')
        ->textInput(['maxlength' => true])
        ->label('Surname <span>(Only status: groom, bride)</span>') ?>

    <?= $form
        ->field($model, 'text')
        ->textarea(['rows' => 3])
        ->label('Text <span>(Only status: groom, bride)</span>') ?>

    <?= $model->image ? Html::img(
        Yii::$app->params['baseUrl'] . $model->image,
        [
            'class' => 'img-responsive',
            'style' => 'width: 300px',
        ]
    ) : ''; ?>

    <?= $form
        ->field($model, 'imageFile')
        ->fileInput() ?>

    <?= $form
        ->field($model, 'status_id')
        ->dropDownList(
            $statuses,
            [
                'prompt' => 'Select',
            ]
        )->label('Status') ?>

    <?= $form
        ->field($model, 'animate_id')
        ->dropDownList(
            $animations,
            [
                'prompt' => 'Select',
            ]
        )->label('Animation') ?>

    <?= $form
        ->field($model, 'delay_id')
        ->dropDownList(
            $delays,
            [
                'prompt' => 'Select',
            ]
        )->label('Delay') ?>

    <?= $form
        ->field($model, 'wedding_id')
        ->dropDownList(
            $weddingIdsList,
            [
                'prompt' => 'Select',
            ]
        ) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
