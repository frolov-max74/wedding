<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\wedding\search\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $weddingIdsList array all Wedding ID */
/* @var $statuses array all Status names */

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <p>
        <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'surname',
                // 'text:ntext',
                [
                    'attribute' => 'image',
                    'label' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->image,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'status_id',
                    'label' => 'Status',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'status_id',
                        $statuses,
                        ['prompt' => 'Select']
                    ),
                    'value' => function ($data) {
                        return $data->status->name;
                    }
                ],
                [
                    'attribute' => 'wedding_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'wedding_id',
                        $weddingIdsList,
                        ['prompt' => 'Select']
                    ),
                    'value' => function ($data) {
                        return $data->wedding->id;
                    },
                ],
                // 'animate_id',
                // 'delay_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>
</div>
