<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Person */
/* @var $statuses array all Status names */
/* @var $animations array all Animate names */
/* @var $delays array all Delay values */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Update Person: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'People', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="person-update">

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
        'weddingIdsList' => $weddingIdsList,
        'animations' => $animations,
        'delays' => $delays,
    ]) ?>

</div>
