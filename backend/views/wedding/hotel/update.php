<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Hotel */
/* @var $weddingIdsList array all Wedding ID */
/* @var $animations array all Animate names */
/* @var $delays array all Delay values */

$this->title = 'Update Hotel: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hotel-update">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
        'animations' => $animations,
        'delays' => $delays,
    ]) ?>

</div>
