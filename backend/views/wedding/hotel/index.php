<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\wedding\search\HotelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Hotels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-index">

    <p>
        <?= Html::a('Create Hotel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                'street_house',
                'city',
                [
                    'attribute' => 'image',
                    'label' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->image,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                // 'animate_id',
                // 'delay_id',
                [
                    'attribute' => 'wedding_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'wedding_id',
                        $weddingIdsList,
                        ['prompt' => 'Select']
                    ),
                    'value' => function ($data) {
                        return $data->wedding->id;
                    },
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

</div>

