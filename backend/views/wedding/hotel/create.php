<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Hotel */
/* @var $weddingIdsList array all Wedding ID */
/* @var $animations array all Animate names */
/* @var $delays array all Delay values */

$this->title = 'Create Hotel';
$this->params['breadcrumbs'][] = ['label' => 'Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-create">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
        'animations' => $animations,
        'delays' => $delays,
    ]) ?>

</div>
