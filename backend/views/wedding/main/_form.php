<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Wedding */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wedding-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'groom_bride')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'story')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'details')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'attending')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'accommodation_top')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'accommodation_down')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'gallery')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'rsvp')->textarea(['rows' => 3]) ?>

    <?= $form
        ->field($model, 'status')
        ->dropDownList([1 => 'Да', 0 => 'Нет'], ['prompt' => 'Выбрать',])
        ->label('Active') ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
