<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Wedding */

$this->title = 'Update Wedding: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Weddings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Wedding: ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wedding-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
