<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Weddings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wedding-index">

    <p>
        <?= Html::a('Create Wedding', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'table table-striped table-bordered table-sm table-inverse'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                'groom_bride:ntext',
                'story:ntext',
                'details:ntext',
                'attending:ntext',
                'accommodation_top:ntext',
                'accommodation_down:ntext',
                'gallery:ntext',
                'rsvp:ntext',
                'status:boolean:Active',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

</div>

