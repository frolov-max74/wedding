<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Wedding */

$this->title = 'Create Wedding';
$this->params['breadcrumbs'][] = ['label' => 'Weddings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wedding-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
