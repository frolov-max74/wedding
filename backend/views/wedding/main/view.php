<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Wedding */

$this->title = 'Wedding: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Weddings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wedding-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'groom_bride:ntext',
            'story:ntext',
            'details:ntext',
            'attending:ntext',
            'accommodation_top:ntext',
            'accommodation_down:ntext',
            'gallery:ntext',
            'rsvp:ntext',
            'status:boolean:Active',
        ],
    ]) ?>

</div>
