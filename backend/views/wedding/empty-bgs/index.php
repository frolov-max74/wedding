<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\wedding\search\EmptyBgsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Empty Backgrounds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empty-bgs-index">

    <p>
        <?= Html::a('Create Empty Backgrounds', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'first_bg',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->first_bg,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'second_bg',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->second_bg,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'third_bg',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->third_bg,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'fourth_bg',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->params['baseUrl'] . $data->fourth_bg,
                            [
                                'class' => 'img-responsive',
                                'style' => 'width: 100px',
                            ]
                        );
                    }
                ],
                [
                    'attribute' => 'wedding_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'wedding_id',
                        $weddingIdsList,
                        ['prompt' => 'Select']
                    ),
                    'value' => function ($data) {
                        return $data->wedding->id;
                    },
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
