<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\EmptyBgs */

$this->title = 'Empty Backgrounds: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="empty-bgs-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'first_bg',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->first_bg,
                        [
                            'class' => 'img-responsive',
                            'width' => '500px',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'second_bg',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->second_bg,
                        [
                            'class' => 'img-responsive',
                            'width' => '500px',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'third_bg',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->third_bg,
                        [
                            'class' => 'img-responsive',
                            'width' => '500px',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'fourth_bg',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->fourth_bg,
                        [
                            'class' => 'img-responsive',
                            'width' => '500px',
                        ]
                    );
                }
            ],
            'wedding_id',
        ],
    ]) ?>

</div>
