<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\EmptyBgs */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Update Empty Backgrounds: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empty-bgs-update">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
    ]) ?>

</div>
