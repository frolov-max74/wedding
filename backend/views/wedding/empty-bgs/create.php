<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\wedding\EmptyBgs */
/* @var $weddingIdsList array all Wedding ID */

$this->title = 'Create Empty Backgrounds';
$this->params['breadcrumbs'][] = ['label' => 'Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="empty-bgs-create">

    <?= $this->render('_form', [
        'model' => $model,
        'weddingIdsList' => $weddingIdsList,
    ]) ?>

</div>
