<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\EmptyBgs */
/* @var $form yii\widgets\ActiveForm */
/* @var $weddingIdsList array all Wedding ID */
?>

<div class="empty-bgs-form">

    <?= Html::tag(
        'p',
        '<span>Необходимо загружать все 4 фоновых изображения сразу!</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Изображения будут обрезаны в размер: 1920X1000 px. Вес загружаемых фотографий не должен превышать 2Mb!</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Во избежание некорректной обрезки, загружайте изображения размером кратным данному размеру.</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Пожалуйста, заранее подготавливайте материал для публикации.</span>'
    ) ?>

    <?php $form = ActiveForm::begin([
    ]); ?>

    <div class="row">
        <?php if ($model->first_bg): ?>
            <div class="col-md-3">
                <?php echo Html::tag(
                    'label',
                    'First Background'
                );
                echo Html::img(
                    Yii::$app->params['baseUrl'] . $model->first_bg,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->second_bg): ?>
            <div class="col-md-3">
                <?php echo Html::tag(
                    'label',
                    'Second Background'
                );
                echo Html::img(
                    Yii::$app->params['baseUrl'] . $model->second_bg,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->third_bg): ?>
            <div class="col-md-3">
                <?php echo Html::tag(
                    'label',
                    'Third Background'
                );
                echo Html::img(
                    Yii::$app->params['baseUrl'] . $model->third_bg,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->fourth_bg): ?>
            <div class="col-md-3">
                <?php echo Html::tag(
                    'label',
                    'Fourth Background'
                );
                echo Html::img(
                    Yii::$app->params['baseUrl'] . $model->fourth_bg,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>
    </div>

    <?= $form
        ->field($model, 'imageFiles[]')
        ->fileInput(['multiple' => true, 'accept' => 'image/*'])
        ->label('Backgrounds') ?>

    <?= $form
        ->field($model, 'wedding_id')
        ->dropDownList(
            $weddingIdsList,
            [
                'prompt' => 'Select',
            ]
        ) ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
