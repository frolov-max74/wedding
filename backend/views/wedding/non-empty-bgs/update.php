<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\NonEmptyBgs */

$this->title = 'Update Non-Empty Backgrounds';
$this->params['breadcrumbs'][] = ['label' => 'Non-Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="non-empty-bgs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
