<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\wedding\NonEmptyBgs */

$this->title = 'Non-Empty Backgrounds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="non-empty-bgs-index">

    <?php if ($model->homePath || $model->marriedPath || $model->thanksPath): ?>
        <p>
            <?= Html::a('Update Non-Empty Backgrounds', ['update'], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php else: ?>
        <p>
            <?= Html::a('Create Non-Empty Backgrounds', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <div class="row">
        <?php if ($model->homePath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Home Background'
                );
                echo Html::img(
                    $model->homeUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->MarriedPath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Get-Married Background'
                );
                echo Html::img(
                    $model->marriedUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->thanksPath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Thanks Background'
                );
                echo Html::img(
                    $model->thanksUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
