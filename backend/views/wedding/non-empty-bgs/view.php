<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\NonEmptyBgs */

$this->title = 'Non-Empty Backgrounds';
$this->params['breadcrumbs'][] = ['label' => 'Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="non-empty-bgs-view">
    <p>
        <?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php if ($model->homePath): ?>
        <div class="row">
            <div class="col-md-2">
                <?php echo Html::tag(
                    'label',
                    'Home Background'
                ); ?>
            </div>
            <div class="col-md-10">
                <?php echo Html::img(
                    $model->homeUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '500px',
                    ]
                ); ?>
                <div class="space-10"></div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($model->MarriedPath): ?>
        <div class="row">
            <div class="col-md-2">
                <?php echo Html::tag(
                    'label',
                    'Get-Married Background'
                ); ?>
            </div>
            <div class="col-md-10">
                <?php echo Html::img(
                    $model->marriedUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '500px',
                    ]
                ); ?>
                <div class="space-10"></div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($model->thanksPath): ?>
        <div class="row">
            <div class="col-md-2">
                <?php echo Html::tag(
                    'label',
                    'Thanks Background'
                ); ?>
            </div>
            <div class="col-md-10">
                <?php echo Html::img(
                    $model->thanksUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '500px',
                    ]
                ); ?>
                <div class="space-10"></div>
            </div>
        </div>
    <?php endif; ?>
</div>
