<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\NonEmptyBgs */

$this->title = 'Create Non-Empty Backgrounds';
$this->params['breadcrumbs'][] = ['label' => 'Non-Empty Backgrounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="non-empty-bgs-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
