<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\NonEmptyBgs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="non-empty-bgs-form">

    <?= Html::tag(
        'p',
        '<span>Необходимо загружать все 3 фоновых изображения ("home", "get-married", "thanks") сразу!</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Изображения будут обрезаны в размер: 1920X1000 px. Вес загружаемых фотографий не должен превышать 2Mb!</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Во избежание некорректной обрезки, загружайте изображения размером кратным данному размеру.</span>'
    ) ?>
    <?= Html::tag(
        'p',
        '<span>Пожалуйста, заранее подготавливайте материал для публикации.</span>'
    ) ?>

    <?php $form = ActiveForm::begin([
    ]); ?>

    <div class="row">
        <?php if ($model->homePath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Home Background'
                );
                echo Html::img(
                    $model->homeUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->MarriedPath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Get-Married Background'
                );
                echo Html::img(
                    $model->marriedUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>

        <?php if ($model->thanksPath): ?>
            <div class="col-md-4">
                <?php echo Html::tag(
                    'label',
                    'Thanks Background'
                );
                echo Html::img(
                    $model->thanksUrl,
                    [
                        'class' => 'img-responsive',
                        'width' => '300px',
                    ]
                ); ?>
            </div>
        <?php endif; ?>
    </div>

    <?= $form
        ->field($model, 'imageFiles[]')
        ->fileInput(['multiple' => true, 'accept' => 'image/*'])
        ->label('Backgrounds') ?>

    <div class="form-group">
        <?= Html::submitButton(
            (
                $model->homePath ||
                $model->marriedPath ||
                $model->thanksPath
            ) ? 'Update' : 'Create',
            [
                'class' => (
                    $model->homePath ||
                    $model->marriedPath ||
                    $model->thanksPath
                ) ? 'btn btn-primary' : 'btn btn-success',
            ]
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
