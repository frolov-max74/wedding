<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Animate */

$this->title = 'Update Animation: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Animations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="animate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
