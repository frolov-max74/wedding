<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Animate */

$this->title = 'Create Animation';
$this->params['breadcrumbs'][] = ['label' => 'Animations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="animate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
