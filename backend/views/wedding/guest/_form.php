<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\wedding\{GendersList, EventsList};

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Guest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'name')
        ->textInput(['maxlength' => true])
        ->label('Имя') ?>

    <?= $form
        ->field($model, 'surname')
        ->textInput(['maxlength' => true])
        ->label('Фамилия') ?>

    <?= $form
        ->field($model, 'email')
        ->textInput(['maxlength' => true]) ?>

    <?= $form
        ->field($model, 'gender')
        ->dropDownList(GendersList::$names, ['prompt' => 'Выбрать',])
        ->label('Пол') ?>

    <?= $form
        ->field($model, 'presence')
        ->dropDownList(EventsList::$names, ['prompt' => 'Выбрать',])
        ->label('Присутствие') ?>

    <?= $form
        ->field($model, 'message')
        ->hiddenInput()
        ->label(false) ?>

    <?= $form
        ->field($model, 'created_at')
        ->hiddenInput()
        ->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
