<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\wedding\{GendersList, EventsList};

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Guest */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Гости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-view">

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этого гостя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'attribute' => 'name',
                'label' => 'Имя',
            ],
            [
                'attribute' => 'surname',
                'label' => 'Фамилия',
            ],
            'email:email',
            [
                'attribute' => 'gender',
                'label' => 'Пол',
                'value' => GendersList::$names[$model->gender],
            ],
            [
                'attribute' => 'presence',
                'label' => 'Присутствие',
                'value' => EventsList::$names[$model->presence],
            ],
            [
                'attribute' => 'message',
                'format' => 'ntext',
                'label' => 'Сообщение',
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'dd-MM-Y HH:mm:ss'],
                'label' => 'Зарегистрирован',
            ],
        ],
    ]) ?>

</div>
