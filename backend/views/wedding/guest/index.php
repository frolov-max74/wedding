<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use common\models\wedding\{GendersList, EventsList};

/* @var $this yii\web\View */
/* @var $searchModel backend\models\wedding\search\GuestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Гости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-index">

    <div class="table-responsive">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                [
                    'attribute' => 'name',
                    'label' => 'Имя',
                ],
                [
                    'attribute' => 'surname',
                    'label' => 'Фамилия',
                ],
                'email:email',
                [
                    'attribute' => 'gender',
                    'label' => 'Пол',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'gender',
                        GendersList::$names,
                        [
                            'prompt' => 'Выбрать',
                        ]
                    ),
                    'value' => function ($data) {
                        return array_search((int)$data->gender, array_flip(GendersList::$names));
                    },
                ],
                [
                    'attribute' => 'presence',
                    'label' => 'Присутствие',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'presence',
                        EventsList::$names,
                        [
                            'prompt' => 'Выбрать',
                        ]
                    ),
                    'value' => function ($data) {
                        return array_search((int)$data->presence, array_flip(EventsList::$names));
                    },
                ],
                // 'message:ntext',
                [
                    'attribute' => 'created_at',
                    'label' => 'Зарегистрирован',
                    'format' => ['date', 'dd-MM-Y HH:mm:ss'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]),
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

</div>
