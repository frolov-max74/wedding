<?php

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Guest */

$this->title = 'Обновить гостя: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Гости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="guest-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
