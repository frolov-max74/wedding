<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\wedding\Story */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Story', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="story-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text:ntext',
            [
                'attribute' => 'date',
                'format' => ['date', 'dd-MM-yyyy'],
            ],
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img(
                        Yii::$app->params['baseUrl'] . $model->image,
                        [
                            'class' => 'img-responsive',
                            'style' => 'width: 300px',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'img_animate_id',
                'label' => 'Image Animation',
                'value' => function ($model) {
                    return $model->imgAnimate->name;
                }
            ],
            [
                'attribute' => 'txt_animate_id',
                'label' => 'Text Animation',
                'value' => function ($model) {
                    return $model->txtAnimate->name;
                }
            ],
            [
                'attribute' => 'img_delay_id',
                'label' => 'Image Delay',
                'value' => function ($model) {
                    return $model->imgDelay->value;
                }
            ],
            [
                'attribute' => 'txt_delay_id',
                'label' => 'Text Delay',
                'value' => function ($model) {
                    return $model->txtDelay->value;
                }
            ],
            'wedding_id',
        ],
    ]) ?>

</div>
